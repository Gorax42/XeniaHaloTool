﻿using ConsoleApp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XeniaHaloTool
{
    public partial class Form1 : Form
    {
        public long CurrentAddress = 0x182000000;

        private Process[] processes = Process.GetProcessesByName("xenia");

        public Form1()
        {
            InitializeComponent();

            comboBox_mapLoad_game.Items.Add("Halo 3");
            comboBox_mapLoad_game.Items.Add("Halo 3 ODST");
            comboBox_mapLoad_game.Items.Add("Halo Reach");

            textBox_memSearch_pattern.Text = "Type here an array to scan.";

            // Check if memory region is indeed at 0x182000000 or 0x282000000, beyond that rip
            processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                label_memSearch_status.Text = "ERROR: Unable to find xenia.exe.";
                return;
            }

            using (var reader = new BinaryReader(new ProcessMemoryStream(processes[0])))
            {
                LabelRestart:
                ulong marker = 0;
                reader.BaseStream.Position = CurrentAddress;
                try
                {
                    marker = reader.ReadUInt64();
                    reader.BaseStream.Position -= 8;

                    if (marker != 0x0000000300905A4D)
                    {
                        CurrentAddress += 0x100000000;

                        if (reader.BaseStream.Position > 0x380000000)
                            return;

                        goto LabelRestart;
                    }

                    label_memSearch_status.Text = $"Detected memory region: 0x{reader.BaseStream.Position:X16}";
                }
                catch (Exception f)
                {
                    CurrentAddress += 0x100000000;

                    if (reader.BaseStream.Position > 0x380000000)
                        return;

                    goto LabelRestart;
                }
            }

        }

        #region Memory stuf
        const int PROCESS_VM_WRITE = 0x0020;
        const int PROCESS_VM_OPERATION = 0x0008;

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(IntPtr hProcess, Int64 lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(IntPtr hProcess, Int64 lpBaseAddress);

        [DllImport("kernel32.dll")]
        public static extern IntPtr CloseHandle(IntPtr hProcess);

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }
        #endregion



        // Forceload tab

        public void AddMaps(string game)
        {
            comboBox_mapLoad_map.Items.Clear();

            if (game.StartsWith("Halo 3 ODST")) // StartsWith: initial name was Halo 3 ODST (unavailable)
            {
                foreach (var a in Data.ODST_maps)
                    comboBox_mapLoad_map.Items.Add(a);
            }
            else if (game.StartsWith("Halo 3"))
            {
                foreach (var a in Data.H3C_maps)
                    comboBox_mapLoad_map.Items.Add(a);
                foreach (var a in Data.H3M_maps)
                    comboBox_mapLoad_map.Items.Add(a);
            }
            else if (game.StartsWith("Halo Reach"))
            {
                foreach (var a in Data.Reach_maps)
                    comboBox_mapLoad_map.Items.Add(a);
            }
        }

        private void comboBox_mapLoad_game_SelectedValueChanged(object sender, EventArgs e)
        {
            // Update maps and gamemode lists on game change
            AddMaps(comboBox_mapLoad_game.SelectedItem.ToString());

            comboBox_mapLoad_gamemode.Items.Clear();

            foreach (var a in Data.GameModeNames)
                comboBox_mapLoad_gamemode.Items.Add(a);

            comboBox_mapLoad_gamemode.SelectedItem = comboBox_mapLoad_gamemode.Items[0];
            comboBox_mapLoad_map.SelectedItem = comboBox_mapLoad_map.Items[0];
        }

        private void button_mapLoad_load_Click(object sender, EventArgs e)
        {
            CurrentAddress = 0x182000000;

            if (comboBox_mapLoad_game.SelectedItem == null)
            {
                label_mapLoad_status.Text = "ERROR: a game must be selected.";
                return;
            }
            if (comboBox_mapLoad_gamemode.SelectedItem == null)
            {
                label_mapLoad_status.Text = "ERROR: a game mode must be selected.";
                return;
            }
            if (comboBox_mapLoad_map.SelectedItem == null)
            {
                label_mapLoad_status.Text = "ERROR: a map must be selected.";
                return;
            }

            label_mapLoad_status.Text = $"Loading {comboBox_mapLoad_map.SelectedItem} as {comboBox_mapLoad_gamemode.SelectedItem} in {comboBox_mapLoad_game.SelectedItem}";

            long addrMapReloadFlag = (long)(CurrentAddress + 0x0000000000BE0A58);
            var addrGameMode = addrMapReloadFlag + 0x12;
            var addrMapname = addrMapReloadFlag + 0x3C;
            var offsetMapnameCheck = 0x34;

            var mapname = comboBox_mapLoad_map.Text.Split(".".ToCharArray()).First();
            var gamemode = comboBox_mapLoad_gamemode.Text.Split(" ".ToCharArray()).First();
            byte gamemodeFlag = 0; // i need to relearn enums asap

            switch (gamemode)
            {
                case "none":
                    gamemodeFlag = 0;
                    break;
                case "campaign":
                    gamemodeFlag = 1;
                    break;
                case "multiplayer":
                    gamemodeFlag = 2;
                    break;
                case "mainmenu":
                    gamemodeFlag = 3;
                    break;
                case "shared":
                    gamemodeFlag = 4;
                    break;
                default:
                    return;
            }

            // Convert map name to a byte list
            var mapnameChars = mapname.ToCharArray();
            var mapnameBytesList = new List<byte>();
            foreach (var b in mapnameChars)
                mapnameBytesList.Add((byte)b);
            var mapnameBytes = mapnameBytesList.ToArray();

            var newMapnameBytes = new byte[16];
            for (int i = 0; i < mapnameBytes.Length; i++)
                newMapnameBytes[i] = mapnameBytes[i];

            if (comboBox_mapLoad_game.SelectedItem.ToString().StartsWith("Halo 3 ODST"))
            { }
            else if (comboBox_mapLoad_game.SelectedItem.ToString().StartsWith("Halo 3"))
            {
                addrMapReloadFlag = (long)(CurrentAddress + 0x000000000095E716);
                addrGameMode = addrMapReloadFlag + 0xC;
                addrMapname = addrMapReloadFlag + 0x36;
                offsetMapnameCheck = 0x2E;
            }
            else if (comboBox_mapLoad_game.SelectedItem.ToString().StartsWith("Halo Reach"))
            {
                addrMapReloadFlag = (long)(CurrentAddress - 0x0000000182000000 + 0x18367F65C);
                addrGameMode = 0; // addrMapReloadFlag + 0xC; // WRONG
                addrMapname = 0; // CurrentAddress - 0x0000000182000000 + 0x18368EEAC;
                offsetMapnameCheck = 0xF848; // WRONG
            }

            int outs = 0;

            // do a check if memory region is correct
            using (var reader = new BinaryReader(new ProcessMemoryStream(processes[0])))
            {
                reader.BaseStream.Position = addrMapReloadFlag + offsetMapnameCheck;
                var buffer = reader.ReadUInt64();
                if (buffer == 0x5c7370616d5c3a64)
                    goto BufferCorrect;
                else
                {
                    label_mapLoad_status.Text = "ERROR hardcoded addresses are invalid for this machine.";
                    return;
                }
            }
            
            BufferCorrect:

            label_mapLoad_status.Text = $"Detected correct memory region. {addrMapReloadFlag:X16}";
            textBox_mapLoad_addr.Text = $"{addrMapReloadFlag:X16}";

            WriteProcessMemory(processes[0].Handle, addrGameMode, new byte[] { 0, gamemodeFlag }, 2, out outs);
            
            WriteProcessMemory(processes[0].Handle, addrMapname, newMapnameBytes, (uint)newMapnameBytes.Length, out outs);
             
            WriteProcessMemory(processes[0].Handle, addrMapReloadFlag, BitConverter.GetBytes(0x1).Reverse().ToArray(), 4, out outs);

        }

        private void button_global_findXenia_Click(object sender, EventArgs e)
        {
            processes = Process.GetProcessesByName("xenia");
            if (processes.Length == 0)
                label_mapLoad_status.Text = "ERROR: no xenia.exe processes detected.";
            else
                label_mapLoad_status.Text = "Reattached to xenia.exe";
        }



        // Memory scan tab

        private void button_memScan_scan_Click(object sender, EventArgs e)
        {
            richTextBox_memScan_results.Text = "";

            var inputSearchArrayString = textBox_memSearch_pattern.Text;

            label_memSearch_status.Text = "Searching...";

            textBox_memScan_initAddr.Text = $"{CurrentAddress:X16}";

            var address = MemoryScanOnce(inputSearchArrayString);

            label_memSearch_status.Text = $"Found at 0x{address:X16}...";
        }

        private void button_memScan_findAll_Click(object sender, EventArgs e)
        {
            richTextBox_memScan_results.Text = "";

            var inputSearchArrayString = textBox_memSearch_pattern.Text;

            label_memSearch_status.Text = "Searching...";

            long address = 0;

            MemoryScanAll(inputSearchArrayString);
        }

        private void button_memScan_testMemProt_Click(object sender, EventArgs e)
        {
            var tempAddr = 0x1BFBEC240;

            Process process;

            processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                label_mapLoad_status.Text = "ERROR: Unable to find xenia.exe.";
                return;
            }

            label_mapLoad_status.Text = "Searching...";

            process = processes[0];

            using (var bin = new BinaryReader(new ProcessMemoryStream(process)))
            {
                bin.BaseStream.Position = tempAddr;
                var buffer = bin.ReadUInt32();

                bin.BaseStream.Position = tempAddr;

                int outs = 0;
                WriteProcessMemory(process.Handle, bin.BaseStream.Position, new byte[] { 0, 0, 0, 0 }, 4, out outs);

                bin.BaseStream.Position = tempAddr;
                buffer = bin.ReadUInt32();

                if (buffer == 0)
                    label_memSearch_status.Text = "SUCCESS";
                else
                    label_memSearch_status.Text = "ERROR: failed to write to memory.";
            }
        }

        private long MemoryScanOld(string inputSearchArrayString)
        {
            var pattern = new List<byte>();

            // Remove spaces from input
            if (inputSearchArrayString.Contains(" "))
            {
                var a = "";
                foreach (var b in inputSearchArrayString)
                {
                    if (b != " ".ToCharArray()[0])
                    {
                        a = $"{a}{b}";
                    }
                }
                inputSearchArrayString = a;
            }

            // Lame conversion to a byte list
            for (int i = 0; i < inputSearchArrayString.Length - 2; i = i + 2)
            {
                var b = $"{inputSearchArrayString[i]}{inputSearchArrayString[i + 1]}";
                byte t;
                byte.TryParse(b, NumberStyles.HexNumber, null, out t);
                pattern.Add(t);
            }

            // Find process
            processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                label_memSearch_status.Text = "ERROR: Unable to find xenia.exe.";
                return 0;
            }

            using (var reader = new BinaryReader(new ProcessMemoryStream(processes[0])))
            {
                reader.BaseStream.Position = CurrentAddress;
                
                long foundAddr = 0;

                while (true)
                {
                    // Read a chunk of data before searching
                    var bufferSize = 0x1000;

                    LabelReread:
                    var buffer = reader.ReadBytes(bufferSize);

                    if (buffer.Length == 0)
                    {
                        reader.BaseStream.Position += bufferSize;
                        goto LabelReread;
                    }

                    var bufferCounter = -1;
                    foreach (var bufferByte in buffer)
                    {
                        bufferCounter++;

                        if (bufferByte != pattern[0])
                            continue;

                        var foundCount = 0;
                        var patternCounter = -1;
                        foreach (var patternByte in pattern)
                        {
                            patternCounter++;

                            if (bufferCounter + patternCounter < buffer.Length)
                            {
                                var newByte = buffer[bufferCounter + patternCounter];
                                if (patternByte != newByte)
                                    goto LabelNoMatch;
                                else
                                    foundCount++;
                            }
                        }

                        if (foundCount != pattern.Count)
                            continue;


                        var tempAddr = reader.BaseStream.Position;
                        foundAddr = reader.BaseStream.Position - bufferSize + bufferCounter;
                        // label_memSearch_status.Text = $"Found at {tempAddr:X8}";

                        richTextBox_memScan_results.Text =
                            $"{richTextBox_memScan_results.Text}" +
                            $"{foundAddr:X8}\n";

                        reader.BaseStream.Position = tempAddr;

                        // Make sure there isnt part of the pattern at the end of the buffer that wouldn't be caught. So go back the size of the pattern, before reading a new chunk for the buffer
                        reader.BaseStream.Position -= pattern.Count;

                        CurrentAddress = reader.BaseStream.Position;

                        return foundAddr;

                        LabelNoMatch:
                        ;
                    }
                }
            }
        }

        private long MemoryScanAll(string inputSearchArrayString)
        {
            var pattern = new List<byte>();

            // Remove spaces from input
            if (inputSearchArrayString.Contains(" "))
            {
                var a = "";
                foreach (var b in inputSearchArrayString)
                {
                    if (b != " ".ToCharArray()[0])
                    {
                        a = $"{a}{b}";
                    }
                }
                inputSearchArrayString = a;
            }

            // Lame conversion to a byte list
            for (int i = 0; i < inputSearchArrayString.Length - 2; i = i + 2)
            {
                var b = $"{inputSearchArrayString[i]}{inputSearchArrayString[i + 1]}";
                byte t;
                byte.TryParse(b, NumberStyles.HexNumber, null, out t);
                pattern.Add(t);
            }

            // Find process
            processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                label_memSearch_status.Text = "ERROR: Unable to find xenia.exe.";
                return 0;
            }

            using (var reader = new BinaryReader(new ProcessMemoryStream(processes[0])))
            {
                reader.BaseStream.Position = CurrentAddress;

                long foundAddr = 0;

                foreach (var addr in addresses)
                {
                    if (addr.Type != 1)
                        continue;

                    var address = addr.Address;
                    var length = addr.Length;

                    reader.BaseStream.Position = address;

                    // Read a chunk of data before searching
                    var bufferSize = length;

                    byte[] buffer;

                    try
                    {
                        buffer = reader.ReadBytes(bufferSize);
                    }
                    catch
                    {
                        Console.WriteLine(
                            $"INVALID {addr.Address:X16}," +
                            $"{addr.Length:X16}," +
                            $"{addr.Type:X2}");
                        continue;
                    }

                    if (buffer.Length == 0)
                        continue;

                    Console.WriteLine(
                        $"Checking {addr.Address:X16}," +
                        $"{addr.Length:X16}," +
                        $"{addr.Type:X2}," +
                        $"{buffer.Length:X16}");

                    var bufferCounter = -1;
                    foreach (var bufferByte in buffer)
                    {
                        bufferCounter++;

                        if (bufferByte != pattern[0])
                            continue;

                        var foundCount = 0;
                        var patternCounter = -1;
                        foreach (var patternByte in pattern)
                        {
                            patternCounter++;

                            if (bufferCounter + patternCounter < buffer.Length)
                            {
                                var newByte = buffer[bufferCounter + patternCounter];
                                if (patternByte != newByte)
                                    goto LabelNoMatch;
                                else
                                    foundCount++;
                            }
                        }

                        if (foundCount != pattern.Count)
                            continue;


                        var tempAddr = reader.BaseStream.Position;
                        foundAddr = reader.BaseStream.Position - bufferSize + bufferCounter;
                        // label_memSearch_status.Text = $"Found at {tempAddr:X8}";

                        richTextBox_memScan_results.Text =
                            $"{richTextBox_memScan_results.Text}" +
                            $"{foundAddr:X8}\n";

                        Console.WriteLine($"Found at {foundAddr:X8}");

                        reader.BaseStream.Position = tempAddr;

                        CurrentAddress = reader.BaseStream.Position;

                        AddressTagsToNull.Add(foundAddr);

                        LabelNoMatch:
                        ;
                    }
                }

                return 0;
            }
        }

        private long MemoryScanOnce(string inputSearchArrayString)
        {
            var pattern = new List<byte>();

            // Remove spaces from input
            if (inputSearchArrayString.Contains(" "))
            {
                var a = "";
                foreach (var b in inputSearchArrayString)
                {
                    if (b != " ".ToCharArray()[0])
                    {
                        a = $"{a}{b}";
                    }
                }
                inputSearchArrayString = a;
            }

            // Lame conversion to a byte list
            for (int i = 0; i < inputSearchArrayString.Length - 2; i = i + 2)
            {
                var b = $"{inputSearchArrayString[i]}{inputSearchArrayString[i + 1]}";
                byte t;
                byte.TryParse(b, NumberStyles.HexNumber, null, out t);
                pattern.Add(t);
            }

            // Find process
            processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                label_memSearch_status.Text = "ERROR: Unable to find xenia.exe.";
                return 0;
            }

            using (var reader = new BinaryReader(new ProcessMemoryStream(processes[0])))
            {
                reader.BaseStream.Position = CurrentAddress;

                long foundAddr = 0;

                foreach (var addr in addresses)
                {
                    if (addr.Type != 1)
                        continue;

                    var address = addr.Address;
                    var length = addr.Length;

                    reader.BaseStream.Position = address;

                    // Read a chunk of data before searching
                    var bufferSize = length;

                    byte[] buffer;

                    try
                    {
                        buffer = reader.ReadBytes(bufferSize);
                    }
                    catch
                    {
                        Console.WriteLine(
                            $"INVALID {addr.Address:X16}," +
                            $"{addr.Length:X16}," +
                            $"{addr.Type:X2}");
                        continue;
                    }

                    if (buffer.Length == 0)
                        continue;

                    Console.WriteLine(
                        $"Checking {addr.Address:X16}," +
                        $"{addr.Length:X16}," +
                        $"{addr.Type:X2}," +
                        $"{buffer.Length:X16}");

                    var bufferCounter = -1;
                    foreach (var bufferByte in buffer)
                    {
                        bufferCounter++;

                        if (bufferByte != pattern[0])
                            continue;

                        var foundCount = 0;
                        var patternCounter = -1;
                        foreach (var patternByte in pattern)
                        {
                            patternCounter++;

                            if (bufferCounter + patternCounter < buffer.Length)
                            {
                                var newByte = buffer[bufferCounter + patternCounter];
                                if (patternByte != newByte)
                                    goto LabelNoMatch;
                                else
                                    foundCount++;
                            }
                        }

                        if (foundCount != pattern.Count)
                            continue;


                        var tempAddr = reader.BaseStream.Position;
                        foundAddr = reader.BaseStream.Position - bufferSize + bufferCounter;
                        // label_memSearch_status.Text = $"Found at {tempAddr:X8}";

                        richTextBox_memScan_results.Text =
                            $"{richTextBox_memScan_results.Text}" +
                            $"{foundAddr:X8}\n";

                        Console.WriteLine($"Found at {foundAddr:X8}");

                        reader.BaseStream.Position = tempAddr;

                        CurrentAddress = reader.BaseStream.Position;

                        AddressTagsToNull.Add(foundAddr);

                        return foundAddr;

                        LabelNoMatch:
                        ;
                    }
                }

                return 0;
            }
        }

        private void textBox_memScan_initAddr_TextChanged(object sender, EventArgs e)
        {
            long longVal = 0;
            if (!long.TryParse(textBox_memScan_initAddr.Text, NumberStyles.HexNumber, null, out longVal))
                label_memSearch_status.Text = $"ERROR: failed to parse {textBox_memScan_initAddr.Text}";

            CurrentAddress = longVal;
        }

        private void button_scan_nullAllShaders(object sender, EventArgs e)
        {
            // null all tags

            int outs;

            foreach (var a in AddressTagsToNull)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }



        // Cache File tab

        private string path_main = "";
        private string cacheTypeString_replace = "replace";
        private string cacheTypeString_null = "null";

        private void button_cache_load_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            openFileDialog1.ShowDialog();

            var folderDestinationPresetsFile = new FileInfo(openFileDialog1.FileName);
            if (!folderDestinationPresetsFile.Exists)
            {
                label_cache_status.Text = $"ERROR: somehow the file doesn't exist.";
                return;
            }

            path_main = openFileDialog1.FileName;

            label_cache_status.Text = $"Added cache file \"{openFileDialog1.FileName.Split("\\".ToCharArray()).Last()}\"";

            foreach (var a in Data.validTagClasses)
                checkedListBox_cache_tagClasses.Items.Add(a);

            comboBox_cache_type.Items.Clear();

            comboBox_cache_type.Items.Add(cacheTypeString_null);
            comboBox_cache_type.Items.Add(cacheTypeString_replace);

            comboBox_cache_type.SelectedItem = comboBox_cache_type.Items[0];

            richTextBox_cache_info.Text =
                "Select tag groups to replace or null.\n" +
                "Replacing should only be used with shaders, such as:" +
                "Replace all rmsh rmhg rmtr with the first valid rmsh.";
        }

        private void label3_Click(object sender, EventArgs e)
        {
            if (path_main == "")
                return;
        }

        private void button_cache_backup_Click(object sender, EventArgs e)
        {
            if (path_main == "")
                return;

            var newPath = $"{path_main.Split(".".ToCharArray()).First()}_backup.map";
            if (File.Exists(newPath))
                File.Delete(newPath);

            try
            {
                File.Copy(path_main, newPath);
            }
            catch (Exception d)
            {
                label_cache_status.Text = $"ERROR: cannot backup {path_main}";
            }

            label_cache_status.Text = $"Copied backup to {newPath}";

        }

        private void button_cache_restore_Click(object sender, EventArgs e)
        {
            var path_backup = $"{path_main.Split(".".ToCharArray()).First()}_backup.map";
            var path_old = $"{path_main.Split(".".ToCharArray()).First()}_old.map";

            if (File.Exists(path_old))
                File.Delete(path_old);

            if (File.Exists(path_main))
                File.Move(path_main, path_old);
            else
            {
                label_cache_status.Text = $"ERROR cannot find the main cache file.";
                return;
            } 

            if (File.Exists(path_backup))
                File.Copy(path_backup, path_main);

            if (File.Exists(path_backup))
                File.Delete(path_backup); 
        }

        private void button_cache_apply_Click(object sender, EventArgs e)
        {
            label_cache_status.Text = $"Applying changes, please wait...";

            ReplaceTags();
        }

        private object ReplaceTags()
        {
            var TagClassesToReplace = new List<string>();

            foreach (var a in checkedListBox_cache_tagClasses.CheckedItems)
            {
                var tag = a.ToString().Split(",".ToCharArray()).First();
                TagClassesToReplace.Add(tag);
            }

            var TagClassesToReplace2 = new List<string>();

            foreach (var patternTagClass in TagClassesToReplace)
            {
                if (!Data.validTagClassesSimple.Contains(patternTagClass))
                    continue;

                TagClassesToReplace2.Add(patternTagClass);
            }

            TagClassesToReplace = TagClassesToReplace2;

            var mapName = path_main.Split(".".ToCharArray()).First().Split("\\".ToCharArray()).Last();

            var patternAddresses = new List<long>();

            using (var reader = new BinaryReader(File.OpenRead(path_main)))
            {
                foreach (var patternTagClass in TagClassesToReplace)
                {
                    reader.BaseStream.Position = 0;

                    while (true)
                    {
                        var inputSearchArrayString = "";
                        foreach (var a in patternTagClass.ToCharArray())
                            inputSearchArrayString = $"{inputSearchArrayString}{(byte)a:X2}";

                        if (patternTagClass.Length == 3)
                            inputSearchArrayString = $"{inputSearchArrayString}20";

                        // Convert search pattern to a list of bytes
                        var pattern = HexStringArrayToHex(inputSearchArrayString);

                        pattern.AddRange(new byte[8]);

                        // Read a chunk of data before searching
                        var bufferSize = 0x1000;
                        var buffer = new byte[0];

                        if (reader.BaseStream.Position > reader.BaseStream.Length - bufferSize)
                            buffer = reader.ReadBytes((int)(reader.BaseStream.Length - reader.BaseStream.Position));
                        else
                            buffer = reader.ReadBytes(bufferSize);

                        if (reader.BaseStream.Position == reader.BaseStream.Length)
                            break;

                        // Make sure there isnt part of the pattern at the end of the buffer that wouldn't be caught. So go back the size of the pattern, before reading a new chunk for the buffer
                        reader.BaseStream.Position -= pattern.Count;

                        var bufferCounter = -1;
                        foreach (var bufferByte in buffer)
                        {
                            bufferCounter++;

                            if (bufferByte != pattern[0])
                                continue;

                            var foundCount = 0;
                            var patternCounter = -1;
                            foreach (var patternByte in pattern)
                            {
                                patternCounter++;

                                if (bufferCounter + patternCounter < buffer.Length)
                                {
                                    if (patternByte != buffer[bufferCounter + patternCounter])
                                        goto LabelNoMatch;
                                    else
                                        foundCount++;
                                }
                            }

                            if (foundCount != pattern.Count)
                                continue;

                            patternAddresses.Add(reader.BaseStream.Position - bufferSize + pattern.Count + bufferCounter);

                            LabelNoMatch:
                            ;
                        }
                    }
                }
            }

            var replacementTag = "FFFFFFFF0000000000000000FFFFFFFF";

            if (comboBox_cache_type.SelectedItem.ToString() == cacheTypeString_replace)
            {
                using (var reader = new BinaryReader(File.OpenRead(path_main)))
                {
                    // Use the first found tag as replacement for all tags
                    foreach (var addr in patternAddresses)
                    {
                        reader.BaseStream.Position = addr;

                        var a = reader.ReadBytes(16);

                        var d = "";
                        foreach (var b in a)
                            d = $"{d}{b:X2}";

                        replacementTag = d;

                        break;
                    }

                    // simply draw all valid tags found, for manual verification
                    foreach (var addr in patternAddresses)
                    {
                        reader.BaseStream.Position = addr;

                        var a = reader.ReadBytes(16);

                        var d = "";
                        foreach (var b in a)
                            d = $"{d}{b:X2}";
                    }
                }
            }

            // Now we should have a list of all addresses where the pattern was found, just rewrite with the new samme
            using (var writer = new BinaryWriter(File.OpenWrite(path_main)))
            {
                foreach (var addr in patternAddresses)
                {
                    writer.BaseStream.Position = addr;

                    writer.Write(HexStringArrayToHex(replacementTag).ToArray());
                }
            }

            label_cache_status.Text = $"Done.";
            return true;
        }

        private static List<byte> HexStringArrayToHex(string inputSearchArrayString)
        {
            // Check for spaces in search pattern
            if (inputSearchArrayString.Contains(" "))
            {
                var a = "";
                foreach (var b in inputSearchArrayString)
                    if (b != " ".ToCharArray()[0])
                        a = $"{a}{b}";

                inputSearchArrayString = a;
            }

            // Check if search pattern is correct
            if (inputSearchArrayString.Length % 2 != 0)
            {
                //Console.WriteLine($"ERROR: input pattern is not correct.");
                return new List<byte>();
            }

            var pattern = new List<byte>();
            for (int i = 0; i < inputSearchArrayString.Length; i = i + 2)
            {
                var b = $"{inputSearchArrayString[i]}{inputSearchArrayString[i + 1]}";
                byte t;
                byte.TryParse(b, NumberStyles.HexNumber, null, out t);
                pattern.Add(t);
            }

            return pattern;
        }

        private void button_cache_selectTagClasses(object sender, EventArgs e)
        {
            var shaderTags = new List<string> { "rmsh", "rmtr", "rmhg", "rmfl", "rmcs", "rmss", "rmzo", "decs", "rmzo", "rmct" };

            var indexes = new List<int>();

            int i = -1;
            foreach (var a in checkedListBox_cache_tagClasses.Items)
            {
                i++;
                if (shaderTags.Contains(a.ToString().Split(",".ToCharArray()).First()))
                    indexes.Add(i);
            }

            foreach (var a in indexes)
                checkedListBox_cache_tagClasses.SetItemChecked(a, true);
        }

        private void button_cache_clearCheckedListBox_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox_cache_tagClasses.Items.Count; i++)
                checkedListBox_cache_tagClasses.SetItemChecked(i, false);
        }



        // Camera
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Point(POINT point)
            {
                return new Point(point.X, point.Y);
            }
        }

        public static Point GetCursorPosition()
        {
            POINT lpPoint;
            GetCursorPos(out lpPoint);

            return lpPoint;
        }

        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out POINT lpPoint);

        private void button_camera_1(object sender, EventArgs e)
        {
            float b1;
            float c1;
            float d1;
            float e1;
            float f1;
            float g1;
            float hLookAngle;
            float vLookAngle;
            Point a;
            int outs = 0;

            processes = Process.GetProcessesByName("xenia");

            var addressMain = 0; // ???

            var addr1 = MemoryScanOnce("62 72 65 61 6B 61 62 6C 65 20 73 75 72 66 61 63 65 20 73 65 74 20 62 72 6F 6B 65 6E");
            addr1 = MemoryScanOnce("62 72 65 61 6B 61 62 6C 65 20 73 75 72 66 61 63 65 20 73 65 74 20 62 72 6F 6B 65 6E");
            addr1 += 0x119D8;

            // read at addr1 += 0x119D4, compare to 0x9C61AABF

            while (true)
            {
                a = GetCursorPosition();

                hLookAngle = (float)Math.PI * ((float)(-a.X + 0) / 3) / 180.0f;
                vLookAngle = (float)Math.PI * ((float)(-a.Y + 500) / 6) / 180.0f;

                b1 = (float)(Math.Cos(hLookAngle) * Math.Cos(vLookAngle));
                c1 = (float)(Math.Sin(hLookAngle) * Math.Cos(vLookAngle));
                d1 = (float)(Math.Sin(vLookAngle));
                e1 = (float)(-Math.Cos(hLookAngle) * Math.Sin(vLookAngle));
                f1 = (float)(-Math.Sin(hLookAngle) * Math.Sin(vLookAngle));
                g1 = (float)(Math.Cos(vLookAngle));

                WriteProcessMemory(processes[0].Handle, addr1 + 0x0, BitConverter.GetBytes(b1).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(processes[0].Handle, addr1 + 0x4, BitConverter.GetBytes(c1).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(processes[0].Handle, addr1 + 0x8, BitConverter.GetBytes(d1).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(processes[0].Handle, addr1 + 0xC, BitConverter.GetBytes(e1).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(processes[0].Handle, addr1 + 0x10, BitConverter.GetBytes(f1).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(processes[0].Handle, addr1 + 0x14, BitConverter.GetBytes(g1).Reverse().ToArray(), 4, out outs);

            }
        }

        private List<item> addresses = new List<item>();
        private List<long> AddressTagsToNull = new List<long>();

        private void button_camera_2(object sender, EventArgs e)
        {
            using (var reader = new BinaryReader(File.OpenRead("c:\\users\\vanguard\\desktop\\test.raw")))
            {
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    addresses.Add(new item
                    {
                        Address = reader.ReadInt64(),
                        Length = reader.ReadInt32(),
                        Type = reader.ReadByte(),
                        Padding = reader.ReadByte(),
                        Padding1 = reader.ReadInt16(),
                    });
                }
            }

            foreach (var a in addresses)
                Console.WriteLine(
                    $"{a.Address:X16}," +
                    $"{a.Length:X16}," +
                    $"{a.Type:X2}");
        }

        private class item
        {
            public long  Address;
            public int   Length;
            public byte  Type;
            public byte  Padding;
            public short Padding1;

        }



        // rmt2 tab

        private void button_rmt2_setOne(object sender, EventArgs e)
        {
            int val = 0;

            for (int i = 0; i < 32; i++)
                if (checkedListBox_tabRmt2_flags.GetItemCheckState(i).ToString() == "Checked")
                    val += (int)Math.Pow(2, i);

            int outs;
            foreach (var a in Data.temp_reach_rmt2Addr_shader)
            {
                WriteProcessMemory(processes[0].Handle, a + 0x10, BitConverter.GetBytes(val).Reverse().ToArray(), 4, out outs);
                Console.WriteLine($"Writing to 0x{(a + 0x10):X16}");
            }
        }

        private void button_rasg_reloadMap_Click(object sender, EventArgs e)
        {
            var addrMapReloadFlag = 0x18367F65C; // reach only
            int outs = 0;
            WriteProcessMemory(processes[0].Handle, addrMapReloadFlag, BitConverter.GetBytes(0x1).Reverse().ToArray(), 4, out outs);
        }

        private void checkedListBox_tab3_flags_SelectedIndexChanged(object sender, EventArgs e)
        {
            int val = 0;

            for (int i = 0; i < 32; i++)
                if (checkedListBox_tabRmt2_flags.GetItemCheckState(i).ToString() == "Checked")
                    val += (int)Math.Pow(2, i);

            int outs;
            WriteProcessMemory(processes[0].Handle, CurrentAddress, BitConverter.GetBytes(val).Reverse().ToArray(), 4, out outs);
        }

        private void tabControl_rmt2_bitflags(object sender, TabControlEventArgs e)
        {
            checkedListBox_tabRmt2_flags.Items.Clear();

            checkedListBox_tabRmt2_flags.Items.Add($"Bit00__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit01__all");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit02__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit03__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit04__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit05__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit06__lighting");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit07__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit08__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit09__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit10__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit11__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit12__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit13__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit14__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit15__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit16__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit17__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit18__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit19__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit20__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit21__camoTransition");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit22__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit23__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit24__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit25__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit26__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit27__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit28__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit29__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit30__");
            checkedListBox_tabRmt2_flags.Items.Add($"Bit31__");

            checkedListBox_tabRmt2_flags.SetItemChecked(1, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(3, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(4, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(5, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(6, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(9, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(10, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(12, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(13, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(14, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(17, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(18, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(19, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(20, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(21, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(22, true);
            checkedListBox_tabRmt2_flags.SetItemChecked(23, true);

            textBox_tabRmt2_address.Text = "1BF0FF3AC";
            CurrentAddress = 0x1BF0FF3AC;
        }


        // rasg reset

        private void button_rasg_reset(object sender, EventArgs e)
        {
            var defaultRasgShaders = "000082117674736800000000000000008C7F23E17069786C00000000000000008C8023E2000082127674736800000000000000008C8123E37069786C00000000000000008C8223E4000082137674736800000000000000008C8323E57069786C00000000000000008C8423E6000082147674736800000000000000008C8523E77069786C00000000000000008C8623E8000082157674736800000000000000008C8723E97069786C00000000000000008C8823EA000082167674736800000000000000008C8923EB7069786C00000000000000008C8A23EC000082177674736800000000000000008C8B23ED7069786C00000000000000008C8C23EE000082187674736800000000000000008C8D23EF7069786C00000000000000008C8E23F0000000B7FFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000FFFFFFFF000082197674736800000000000000008C8F23F17069786C00000000000000008C9023F20000821A7674736800000000000000008C9123F37069786C00000000000000008C9223F40000821B7674736800000000000000008C9323F57069786C00000000000000008C9423F60000821C7674736800000000000000008C9523F77069786C00000000000000008C9623F80000821D7674736800000000000000008C9723F97069786C00000000000000008C9823FA0000821E7674736800000000000000008C9923FB7069786C00000000000000008C9A23FC0000821F7674736800000000000000008C9B23FD7069786C00000000000000008C9C23FE000082207674736800000000000000008C9D23FF7069786C00000000000000008C9E2400000082217674736800000000000000008C9F24017069786C00000000000000008CA02402000082227674736800000000000000008CA124037069786C00000000000000008CA22404000000B7FFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000FFFFFFFF000046B07674736800000000000000008CA324057069786C00000000000000008CA4240600005E297674736800000000000000008CA524077069786C00000000000000008CA62408000082237674736800000000000000008CA724097069786C00000000000000008CA8240A000082247674736800000000000000008CA9240B7069786C00000000000000008CAA240C000082257674736800000000000000008CAB240D7069786C00000000000000008CAC240E000000B7FFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000FFFFFFFF000082267674736800000000000000008CAD240F7069786C00000000000000008CAE2410000082277674736800000000000000008CAF24117069786C00000000000000008CB02412000082287674736800000000000000008CB124137069786C00000000000000008CB22414000082297674736800000000000000008CB324157069786C00000000000000008CB424160000822A7674736800000000000000008CB524177069786C00000000000000008CB624180000822B7674736800000000000000008CB724197069786C00000000000000008CB8241A0000822C7674736800000000000000008CB9241B7069786C00000000000000008CBA241C0000822D7674736800000000000000008CBB241D7069786C00000000000000008CBC241E0000822E7674736800000000000000008CBD241F7069786C00000000000000008CBE24200000822F7674736800000000000000008CBF24217069786C00000000000000008CC02422000082307674736800000000000000008CC124237069786C00000000000000008CC22424000082317674736800000000000000008CC324257069786C00000000000000008CC42426000082327674736800000000000000008CC524277069786C00000000000000008CC62428000082337674736800000000000000008CC724297069786C00000000000000008CC8242A000082347674736800000000000000008CC9242B7069786C00000000000000008CCA242C000082357674736800000000000000008CCB242D7069786C00000000000000008CCC242E000082367674736800000000000000008CCD242F7069786C00000000000000008CCE2430000082377674736800000000000000008CCF24317069786C00000000000000008CD02432000082387674736800000000000000008CD124337069786C00000000000000008CD22434000082397674736800000000000000008CD324357069786C00000000000000008CD4243600000AC47674736800000000000000008CD524377069786C00000000000000008CD624380000823A7674736800000000000000008CD724397069786C00000000000000008CD8243A0000823B7674736800000000000000008CD9243B7069786C00000000000000008CDA243C0000823C7674736800000000000000008CDB243D7069786C00000000000000008CDC243E0000823D7674736800000000000000008CDD243F7069786C00000000000000008CDE24400000823E7674736800000000000000008CDF24417069786C00000000000000008CE024420000823F7674736800000000000000008CE124437069786C00000000000000008CE22444000082407674736800000000000000008CE324457069786C00000000000000008CE42446000082417674736800000000000000008CE524477069786C00000000000000008CE62448000082427674736800000000000000008CE724497069786C00000000000000008CE8244A000082437674736800000000000000008CE9244B7069786C00000000000000008CEA244C000082447674736800000000000000008CEB244D7069786C00000000000000008CEC244E000082457674736800000000000000008CED244F7069786C00000000000000008CEE2450000082467674736800000000000000008CEF24517069786C00000000000000008CF02452000082477674736800000000000000008CF124537069786C00000000000000008CF22454000082487674736800000000000000008CF324557069786C00000000000000008CF42456000082497674736800000000000000008CF524577069786C00000000000000008CF624580000824A7674736800000000000000008CF724597069786C00000000000000008CF8245A0000824B7674736800000000000000008CF9245B7069786C00000000000000008CFA245C0000824C7674736800000000000000008CFB245D7069786C00000000000000008CFC245E0000824D7674736800000000000000008CFD245F7069786C00000000000000008CFE2460000000B7FFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000FFFFFFFF0000824E7674736800000000000000008CFF24617069786C00000000000000008D0024620000824F7674736800000000000000008D0124637069786C00000000000000008D022464000082507674736800000000000000008D0324657069786C00000000000000008D042466000082517674736800000000000000008D0524677069786C00000000000000008D062468000082527674736800000000000000008D0724697069786C00000000000000008D08246A000082537674736800000000000000008D09246B7069786C00000000000000008D0A246C000082547674736800000000000000008D0B246D7069786C00000000000000008D0C246E000082557674736800000000000000008D0D246F7069786C00000000000000008D0E2470000082567674736800000000000000008D0F24717069786C00000000000000008D102472000082577674736800000000000000008D1124737069786C00000000000000008D122474000082587674736800000000000000008D1324757069786C00000000000000008D142476000082597674736800000000000000008D1524777069786C00000000000000008D162478000082407674736800000000000000008CE324457069786C00000000000000008CE424460000825A7674736800000000000000008D1724797069786C00000000000000008D18247A0000825B7674736800000000000000008D19247B7069786C00000000000000008D1A247C0000825C7674736800000000000000008D1B247D7069786C00000000000000008D1C247E0000825D7674736800000000000000008D1D247F7069786C00000000000000008D1E24800000825E7674736800000000000000008D1F24817069786C00000000000000008D2024820000825F7674736800000000000000008D2124837069786C00000000000000008D22248400008260767473680000000000000000FFFFFFFF7069786C0000000000000000FFFFFFFF000082617674736800000000000000008D2524877069786C00000000000000008D262488000082627674736800000000000000008D2724897069786C00000000000000008D28248A000082637674736800000000000000008D29248B7069786C00000000000000008D2A248C000000B7FFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000FFFFFFFF00008264767473680000000000000000FFFFFFFF7069786C0000000000000000FFFFFFFF000082657674736800000000000000008D2D248F7069786C00000000000000008D2E2490000082667674736800000000000000008D2F24917069786C00000000000000008D302492000082677674736800000000000000008D3124937069786C00000000000000008D322494000082687674736800000000000000008D3324957069786C00000000000000008D342496000082697674736800000000000000008D3524977069786C00000000000000008D3624980000826A7674736800000000000000008D3724997069786C00000000000000008D38249A0000826B7674736800000000000000008D39249B7069786C00000000000000008D3A249C0000826C7674736800000000000000008D3B249D7069786C00000000000000008D3C249E0000826D7674736800000000000000008D3D249F7069786C00000000000000008D3E24A00000826E7674736800000000000000008D3F24A17069786C00000000000000008D4024A20000826F7674736800000000000000008D4124A37069786C00000000000000008D4224A4000082707674736800000000000000008D4324A57069786C00000000000000008D4424A6000082717674736800000000000000008D4524A77069786C00000000000000008D4624A8000082727674736800000000000000008D4724A97069786C00000000000000008D4824AA000082737674736800000000000000008D4924AB7069786C00000000000000008D4A24AC000082747674736800000000000000008D4B24AD7069786C00000000000000008D4C24AE000082757674736800000000000000008D4D24AF7069786C00000000000000008D4E24B0000082767674736800000000000000008D4F24B17069786C00000000000000008D5024B2000082777674736800000000000000008D5124B37069786C00000000000000008D5224B4000082787674736800000000000000008D5324B57069786C00000000000000008D5424B6000082797674736800000000000000008D5524B77069786C00000000000000008D5624B80000827A7674736800000000000000008D5724B97069786C00000000000000008D5824BA0000827B7674736800000000000000008D5924BB7069786C00000000000000008D5A24BC0000827C7674736800000000000000008D5B24BD7069786C00000000000000008D5C24BE0000827D7674736800000000000000008D5D24BF7069786C00000000000000008D5E24C00000827E7674736800000000000000008D5F24C17069786C00000000000000008D6024C20000827F7674736800000000000000008D6124C37069786C00000000000000008D6224C4000082807674736800000000000000008D6324C57069786C00000000000000008D6424C6000082817674736800000000000000008D6524C77069786C00000000000000008D6624C8000082827674736800000000000000008D6724C97069786C00000000000000008D6824CA000082837674736800000000000000008D6924CB7069786C00000000000000008D6A24CC000082847674736800000000000000008D6B24CD7069786C00000000000000008D6C24CE000082857674736800000000000000008D6D24CF7069786C00000000000000008D6E24D0000082867674736800000000000000008D6F24D17069786C00000000000000008D7024D2000082877674736800000000000000008D7124D37069786C00000000000000008D7224D4000082887674736800000000000000008D7324D57069786C00000000000000008D7424D6000082897674736800000000000000008D7524D77069786C00000000000000008D7624D80000828A7674736800000000000000008D7724D97069786C00000000000000008D7824DA0000828B7674736800000000000000008D7924DB7069786C00000000000000008D7A24DC0000828C7674736800000000000000008D7B24DD7069786C00000000000000008D7C24DE0000828D7674736800000000000000008D7D24DF7069786C00000000000000008D7E24E00000828E7674736800000000000000008D7F24E17069786C00000000000000008D8024E20000828F7674736800000000000000008D8124E37069786C00000000000000008D8224E4000082907674736800000000000000008D8324E57069786C00000000000000008D8424E6000082917674736800000000000000008D8524E77069786C00000000000000008D8624E8000082927674736800000000000000008D8724E97069786C00000000000000008D8824EA000082937674736800000000000000008D8924EB7069786C00000000000000008D8A24EC000082947674736800000000000000008D8B24ED7069786C00000000000000008D8C24EE000082957674736800000000000000008D8D24EF7069786C00000000000000008D8E24F0000082967674736800000000000000008D8F24F17069786C00000000000000008D9024F2000082977674736800000000000000008D9124F37069786C00000000000000008D9224F4000082987674736800000000000000008D9324F57069786C00000000000000008D9424F6000082997674736800000000000000008D9524F77069786C00000000000000008D9624F80000829A7674736800000000000000008D9724F97069786C00000000000000008D9824FA0000829B7674736800000000000000008D9924FB7069786C00000000000000008D9A24FC0000829C7674736800000000000000008D9B24FD7069786C00000000000000008D9C24FE0000829D7674736800000000000000008D9D24FF7069786C00000000000000008D9E25000000829E7674736800000000000000008D9F25017069786C00000000000000008DA025020000829F7674736800000000000000008DA125037069786C00000000000000008DA22504000082A07674736800000000000000008DA325057069786C00000000000000008DA42506000082A17674736800000000000000008DA525077069786C00000000000000008DA62508000082A27674736800000000000000008DA725097069786C00000000000000008DA8250A000082A37674736800000000000000008DA9250B7069786C00000000000000008DAA250C000082A47674736800000000000000008DAB250D7069786C00000000000000008DAC250E000082A57674736800000000000000008DAD250F7069786C00000000000000008DAE2510000082A67674736800000000000000008DAD250F7069786C00000000000000008DAF2511000082A77674736800000000000000008DB025127069786C00000000000000008DB12513000082A87674736800000000000000008DB225147069786C00000000000000008DB32515000082A97674736800000000000000008DB425167069786C00000000000000008DB52517000082AA7674736800000000000000008DB625187069786C00000000000000008DB72519000082AB7674736800000000000000008DB8251A7069786C00000000000000008DB9251B000082AC7674736800000000000000008DBA251C7069786C00000000000000008DBB251D00000010";

            var a = HexStringArrayToHex(defaultRasgShaders);

            int outs;

            WriteProcessMemory(processes[0].Handle, CurrentAddress, a.ToArray(), (uint)a.Count, out outs);

        }

        private void button_rasg_apply(object sender, EventArgs e)
        {
            if (Data.Reach_rasg_shaderNames.Count != Data.Reach_rasg_shaders.Count)
                throw new Exception();

            var replacementShader = 0;

            for (int i = 0; i < Data.Reach_rasg_shaderNames.Count; i++)
            {
                if (checkedListBox_rasg_shaderReplacement.GetItemCheckState(i).ToString() == "Checked")
                {
                    replacementShader = i;
                    break;
                }
            }

            var replacement = HexStringArrayToHex(Data.Reach_rasg_shaders[replacementShader]);

            for (int i = 0; i < Data.Reach_rasg_shaderNames.Count; i++)
            {
                if (checkedListBox_rasg_shaders_selection.GetItemCheckState(i).ToString() == "Checked")
                {
                    var addr = CurrentAddress + 0x24 * i;
                    int outs;
                    Console.WriteLine($"0x{addr:X16} {replacement}");
                    WriteProcessMemory(processes[0].Handle, addr, replacement.ToArray(), (uint)replacement.Count, out outs);
                }
            }
        }

        private void button_rasg_search_Click(object sender, EventArgs e)
        {
            var inputSearchArrayString = textBox_tag_search.Text;

            label_memSearch_status.Text = "Searching...";

            // var address = MemoryScanOnce(inputSearchArrayString);
            CurrentAddress = 0x1BFB693C0;

            textBox_tag_search.Text = $"{CurrentAddress:X16}";

            label_tag_status.Text = $"Found at 0x{CurrentAddress:X16}...";

            Items2 = new List<string>();

            try
            {
                using (var reader = new BinaryReader(new ProcessMemoryStream(processes[0])))
                {
                }
            }
            catch
            {
                Console.WriteLine($"ERROR");
                return;
            }

            using (var reader = new BinaryReader(new ProcessMemoryStream(processes[0])))
            {
                reader.BaseStream.Position = CurrentAddress;

                for (int i = 0; i < 164; i++)
                {
                    var a = reader.ReadBytes(0x24);
                    var d = "";
                    foreach (var b in a)
                    {
                        d = $"{d}{b:X2}";
                    }

                    Items2.Add(d);
                }
            }

            var j = -1;
            foreach (var a in Items2)
            {
                j++;
                comboBox_tag_selection.Items.Add($"{Data.Reach_rasg_shaderNames[j]},{a}");
            }

            j = -1;
            foreach (var a in Items2)
            {
                j++;
                comboBox_tag_replacement.Items.Add($"{Data.Reach_rasg_shaderNames[j]},{a}");
            }

            comboBox_tag_replacement.SelectedItem = comboBox_tag_replacement.Items[0];
            comboBox_tag_selection.SelectedItem = comboBox_tag_selection.Items[0];

            foreach (var a in Data.Reach_rasg_shaderNames)
            {
                checkedListBox_rasg_shaders_selection.Items.Add(a);
            }
            foreach (var a in Data.Reach_rasg_shaderNames)
            {
                checkedListBox_rasg_shaderReplacement.Items.Add(a);
            }
        }

        private List<string> Items2 = new List<string>();

        private void comboBox_rasg_selection_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tag_selectedIndex = comboBox_tag_selection.SelectedIndex;

            numericUpDown1.Value = tag_selectedIndex;

            int outs;

            var addr = CurrentAddress + 0x24 * tag_selectedIndex;
            var a = HexStringArrayToHex(comboBox_tag_replacement.SelectedItem.ToString().Split(",".ToCharArray()).Last());

            if (checkBox_tag_autoPoke.Checked)
                WriteProcessMemory(processes[0].Handle, addr, a.ToArray(), 0x24, out outs);
        }

        private void comboBox_rasg_replacement_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tag_replacemtIndex = comboBox_tag_replacement.SelectedIndex;
            numericUpDown2.Value = tag_replacemtIndex;

            var tag_selectedIndex = comboBox_tag_selection.SelectedIndex;

            int outs;

            var addr = CurrentAddress + 0x24 * tag_selectedIndex;
            var a = HexStringArrayToHex(comboBox_tag_replacement.SelectedItem.ToString().Split(",".ToCharArray()).Last());

            if (checkBox_tag_autoPoke.Checked)
                WriteProcessMemory(processes[0].Handle, addr, a.ToArray(), 0x24, out outs);

        }

        private void textBox_rasg_address_TextChanged(object sender, EventArgs e)
        {
            long longVal = 0;
            if (!long.TryParse(textBox_tabRmt2_address.Text, NumberStyles.HexNumber, null, out longVal))
                ; // label_memSearch_status.Text = $"ERROR: failed to parse {textBox_memScan_initAddr.Text}";

            CurrentAddress = longVal;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox_rasg_shaders_selection.Items.Count; i++)
                checkedListBox_rasg_shaders_selection.SetItemChecked(i, false);
            for (int i = 0; i < checkedListBox_rasg_shaderReplacement.Items.Count; i++)
                checkedListBox_rasg_shaderReplacement.SetItemChecked(i, false);
        }

        private void checkedListBox_rasg_shaders_selection_SelectedIndexChanged(object sender, EventArgs e)
        {
            // var a = (System.Windows.Forms.CheckedListBox)sender;
            // var b = a.SelectedItem.ToString();
            // 
            // for (int i = 0; i < checkedListBox_rasg_shaders_selection.Items.Count; i++)
            // {
            //     if (checkedListBox_rasg_shaders_selection.Items[i].ToString() == b)
            //     {
            //         checkedListBox_rasg_shaders_selection.SetItemChecked(i, true);
            //         break;
            //     }
            // }
        }

        public byte[] ReplacementTag = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            // 0xE1, 0x92, 0x00, 0x1C };
            0x8D, 0xF7, 0x1A, 0xE2};

        private void button6_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmsh_sword)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmgl)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmfl)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmtr)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmhg)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmw)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmd)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            int outs;

            foreach (var a in Data.Reach_shadersAddr_rmsh_sword)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmgl)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmfl)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmtr)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmhg)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmw)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmd)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button_tabShaders_reloadMap_Click(object sender, EventArgs e)
        {
            var addrMapReloadFlag = 0x18367F65C; // reach only
            int outs = 0;
            WriteProcessMemory(processes[0].Handle, addrMapReloadFlag, BitConverter.GetBytes(0x1).Reverse().ToArray(), 4, out outs);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            int outs;
            foreach (var a in Data.Reach_shadersAddr_rmsh)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmgl)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmfl)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmtr)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmhg)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmw)
                WriteProcessMemory(processes[0].Handle, a, new byte[]
                {
                    0xFF, 0xFF, 0xFF, 0xFF,
                    0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xFF, 0xFF, 0xFF
                }, 0x10, out outs);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            

            int outs;

            foreach (var a in Data.Reach_shadersAddr_rmsh)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmgl)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmfl)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmtr)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmhg)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmw)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);

            foreach (var a in Data.Reach_shadersAddr_rmd)
                WriteProcessMemory(processes[0].Handle, a, ReplacementTag, 0x10, out outs);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_shaders_replacement.SelectedItem == null)
                return;

            var a = comboBox_shaders_replacement.SelectedItem.ToString().Split(",".ToCharArray())[1];
            ReplacementTag[12] = HexStringArrayToHex(a)[0];
            ReplacementTag[13] = HexStringArrayToHex(a)[1];
            ReplacementTag[14] = HexStringArrayToHex(a)[2];
            ReplacementTag[15] = HexStringArrayToHex(a)[3];

            int outs;
            if (checkBox_shaders_autopoke.Checked)
                foreach (var b in Data.Reach_shadersAddr_rmsh_sword)
                    WriteProcessMemory(processes[0].Handle, b, ReplacementTag, 0x10, out outs);
        }

        private void tabControl_MouseClick(object sender, MouseEventArgs e)
        {
            var items_ = new List<string>();
            using (var csvStream = File.OpenRead(@"D:\FOLDERS\Xenia\ISO\HaloReach\20_sword_slayer_tags.csv"))
            using (var csvReader = new StreamReader(csvStream))
            {
                var line = "";

                while (line != null)
                {
                    line = csvReader.ReadLine();

                    if (line == null)
                        break;

                    var items = line.Split(",".ToCharArray());

                    var class_ = items[0];
                    var ID = items[1];
                    var tagname = items[2];

                    switch(class_)
                    {
                        case "rmsh":
                        case "rmgl":
                        case "rmfl":
                        case "rmtr":
                        case "rmhg":
                        case "rmw":
                        case "rmd":
                            items_.Add(line);
                            break;
                    }

                }
            }

            foreach (var a in items_)
                comboBox_shaders_replacement.Items.Add(a);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            ;
        }

        private void button_tab3_setAll_Click(object sender, EventArgs e)
        {

        }

        private void button_tabRmt2_reloadMap_Click(object sender, EventArgs e)
        {
            var addrMapReloadFlag = 0x18367F65C; // reach only
            int outs = 0;
            WriteProcessMemory(processes[0].Handle, addrMapReloadFlag, BitConverter.GetBytes(0x1).Reverse().ToArray(), 4, out outs);
        }

        private void button_tabRmt2_clearFlags_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void checkedListBox_rasg_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button_tabRmt2_clearFlags_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < checkedListBox_tabRmt2_flags.Items.Count; i++)
                checkedListBox_tabRmt2_flags.SetItemChecked(i, false);
        }
    }
}
