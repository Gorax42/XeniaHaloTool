﻿namespace XeniaHaloTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button button_memScan_findFirst;
            System.Windows.Forms.Button button_memScan_findAll;
            this.label_memScan_initAddr = new System.Windows.Forms.Label();
            this.textBox_memScan_initAddr = new System.Windows.Forms.TextBox();
            this.groupBox_memScan_results = new System.Windows.Forms.GroupBox();
            this.richTextBox_memScan_results = new System.Windows.Forms.RichTextBox();
            this.button_memScan_stop = new System.Windows.Forms.Button();
            this.textBox_memSearch_pattern = new System.Windows.Forms.TextBox();
            this.groupBox_memSearch_status = new System.Windows.Forms.GroupBox();
            this.label_memSearch_status = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage_load = new System.Windows.Forms.TabPage();
            this.button_global_findXenia = new System.Windows.Forms.Button();
            this.comboBox_mapLoad_game = new System.Windows.Forms.ComboBox();
            this.button_mapLoad_load = new System.Windows.Forms.Button();
            this.groupBox_mapLoad_status = new System.Windows.Forms.GroupBox();
            this.label_mapLoad_status = new System.Windows.Forms.Label();
            this.label_mapLoad_addr = new System.Windows.Forms.Label();
            this.label_mapLoad_game = new System.Windows.Forms.Label();
            this.textBox_mapLoad_addr = new System.Windows.Forms.TextBox();
            this.label_mapLoad_map = new System.Windows.Forms.Label();
            this.label_mapLoad_gamemode = new System.Windows.Forms.Label();
            this.comboBox_mapLoad_map = new System.Windows.Forms.ComboBox();
            this.comboBox_mapLoad_gamemode = new System.Windows.Forms.ComboBox();
            this.tabPage_memScan = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPage_cache = new System.Windows.Forms.TabPage();
            this.button_cache_clearCheckedListBox = new System.Windows.Forms.Button();
            this.button_cache_selectShaders = new System.Windows.Forms.Button();
            this.button_cache_restore = new System.Windows.Forms.Button();
            this.button_cache_backup = new System.Windows.Forms.Button();
            this.richTextBox_cache_info = new System.Windows.Forms.RichTextBox();
            this.button_cache_apply = new System.Windows.Forms.Button();
            this.comboBox_cache_type = new System.Windows.Forms.ComboBox();
            this.checkedListBox_cache_tagClasses = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_cache_status = new System.Windows.Forms.Label();
            this.button_cache_load = new System.Windows.Forms.Button();
            this.tabPage_camera = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage_rasg = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.checkedListBox_rasg_shaderReplacement = new System.Windows.Forms.CheckedListBox();
            this.checkedListBox_rasg_shaders_selection = new System.Windows.Forms.CheckedListBox();
            this.button_rmt2_reloadMap = new System.Windows.Forms.Button();
            this.checkBox_tag_autoPoke = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox_tag_status = new System.Windows.Forms.GroupBox();
            this.label_tag_status = new System.Windows.Forms.Label();
            this.button_tag_replace = new System.Windows.Forms.Button();
            this.comboBox_tag_replacement = new System.Windows.Forms.ComboBox();
            this.comboBox_tag_selection = new System.Windows.Forms.ComboBox();
            this.textBox_tag_search = new System.Windows.Forms.TextBox();
            this.button_tag_search = new System.Windows.Forms.Button();
            this.tabPage_rmt2 = new System.Windows.Forms.TabPage();
            this.button_tabRmt2_reloadMap = new System.Windows.Forms.Button();
            this.button_tabRmt2_setOne = new System.Windows.Forms.Button();
            this.checkedListBox_tabRmt2_flags = new System.Windows.Forms.CheckedListBox();
            this.textBox_tabRmt2_address = new System.Windows.Forms.TextBox();
            this.tabPage_shaders = new System.Windows.Forms.TabPage();
            this.checkBox_shaders_autopoke = new System.Windows.Forms.CheckBox();
            this.comboBox_shaders_replacement = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button_tabRmt2_clearFlags = new System.Windows.Forms.Button();
            button_memScan_findFirst = new System.Windows.Forms.Button();
            button_memScan_findAll = new System.Windows.Forms.Button();
            this.groupBox_memScan_results.SuspendLayout();
            this.groupBox_memSearch_status.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage_load.SuspendLayout();
            this.groupBox_mapLoad_status.SuspendLayout();
            this.tabPage_memScan.SuspendLayout();
            this.tabPage_cache.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage_camera.SuspendLayout();
            this.tabPage_rasg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox_tag_status.SuspendLayout();
            this.tabPage_rmt2.SuspendLayout();
            this.tabPage_shaders.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_memScan_findFirst
            // 
            button_memScan_findFirst.Location = new System.Drawing.Point(6, 32);
            button_memScan_findFirst.Name = "button_memScan_findFirst";
            button_memScan_findFirst.Size = new System.Drawing.Size(105, 23);
            button_memScan_findFirst.TabIndex = 5;
            button_memScan_findFirst.Text = "Find first";
            button_memScan_findFirst.UseVisualStyleBackColor = true;
            button_memScan_findFirst.Click += new System.EventHandler(this.button_memScan_scan_Click);
            // 
            // button_memScan_findAll
            // 
            button_memScan_findAll.Location = new System.Drawing.Point(117, 32);
            button_memScan_findAll.Name = "button_memScan_findAll";
            button_memScan_findAll.Size = new System.Drawing.Size(105, 23);
            button_memScan_findAll.TabIndex = 11;
            button_memScan_findAll.Text = "Find All";
            button_memScan_findAll.UseVisualStyleBackColor = true;
            button_memScan_findAll.Click += new System.EventHandler(this.button_memScan_findAll_Click);
            // 
            // label_memScan_initAddr
            // 
            this.label_memScan_initAddr.AutoSize = true;
            this.label_memScan_initAddr.Location = new System.Drawing.Point(116, 65);
            this.label_memScan_initAddr.Name = "label_memScan_initAddr";
            this.label_memScan_initAddr.Size = new System.Drawing.Size(109, 13);
            this.label_memScan_initAddr.TabIndex = 10;
            this.label_memScan_initAddr.Text = "Initial Search Address";
            // 
            // textBox_memScan_initAddr
            // 
            this.textBox_memScan_initAddr.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_memScan_initAddr.Location = new System.Drawing.Point(6, 61);
            this.textBox_memScan_initAddr.Name = "textBox_memScan_initAddr";
            this.textBox_memScan_initAddr.Size = new System.Drawing.Size(105, 20);
            this.textBox_memScan_initAddr.TabIndex = 10;
            this.textBox_memScan_initAddr.Text = "0000000000000000";
            this.textBox_memScan_initAddr.TextChanged += new System.EventHandler(this.textBox_memScan_initAddr_TextChanged);
            // 
            // groupBox_memScan_results
            // 
            this.groupBox_memScan_results.Controls.Add(this.richTextBox_memScan_results);
            this.groupBox_memScan_results.Location = new System.Drawing.Point(6, 87);
            this.groupBox_memScan_results.Name = "groupBox_memScan_results";
            this.groupBox_memScan_results.Size = new System.Drawing.Size(142, 338);
            this.groupBox_memScan_results.TabIndex = 7;
            this.groupBox_memScan_results.TabStop = false;
            this.groupBox_memScan_results.Text = "Results";
            // 
            // richTextBox_memScan_results
            // 
            this.richTextBox_memScan_results.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_memScan_results.Location = new System.Drawing.Point(6, 19);
            this.richTextBox_memScan_results.Name = "richTextBox_memScan_results";
            this.richTextBox_memScan_results.Size = new System.Drawing.Size(130, 313);
            this.richTextBox_memScan_results.TabIndex = 5;
            this.richTextBox_memScan_results.Text = "";
            // 
            // button_memScan_stop
            // 
            this.button_memScan_stop.Location = new System.Drawing.Point(228, 32);
            this.button_memScan_stop.Name = "button_memScan_stop";
            this.button_memScan_stop.Size = new System.Drawing.Size(125, 23);
            this.button_memScan_stop.TabIndex = 6;
            this.button_memScan_stop.Text = "Test memory protection";
            this.button_memScan_stop.UseVisualStyleBackColor = true;
            this.button_memScan_stop.Click += new System.EventHandler(this.button_memScan_testMemProt_Click);
            // 
            // textBox_memSearch_pattern
            // 
            this.textBox_memSearch_pattern.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_memSearch_pattern.Location = new System.Drawing.Point(6, 6);
            this.textBox_memSearch_pattern.Name = "textBox_memSearch_pattern";
            this.textBox_memSearch_pattern.Size = new System.Drawing.Size(392, 20);
            this.textBox_memSearch_pattern.TabIndex = 2;
            // 
            // groupBox_memSearch_status
            // 
            this.groupBox_memSearch_status.Controls.Add(this.label_memSearch_status);
            this.groupBox_memSearch_status.Location = new System.Drawing.Point(6, 431);
            this.groupBox_memSearch_status.Name = "groupBox_memSearch_status";
            this.groupBox_memSearch_status.Size = new System.Drawing.Size(542, 35);
            this.groupBox_memSearch_status.TabIndex = 1;
            this.groupBox_memSearch_status.TabStop = false;
            this.groupBox_memSearch_status.Text = "Status";
            // 
            // label_memSearch_status
            // 
            this.label_memSearch_status.AutoSize = true;
            this.label_memSearch_status.Location = new System.Drawing.Point(6, 16);
            this.label_memSearch_status.Name = "label_memSearch_status";
            this.label_memSearch_status.Size = new System.Drawing.Size(24, 13);
            this.label_memSearch_status.TabIndex = 1;
            this.label_memSearch_status.Text = "Idle";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage_load);
            this.tabControl.Controls.Add(this.tabPage_memScan);
            this.tabControl.Controls.Add(this.tabPage_cache);
            this.tabControl.Controls.Add(this.tabPage_camera);
            this.tabControl.Controls.Add(this.tabPage_rasg);
            this.tabControl.Controls.Add(this.tabPage_rmt2);
            this.tabControl.Controls.Add(this.tabPage_shaders);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(562, 498);
            this.tabControl.TabIndex = 3;
            this.tabControl.Tag = "";
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_rmt2_bitflags);
            // 
            // tabPage_load
            // 
            this.tabPage_load.Controls.Add(this.button_global_findXenia);
            this.tabPage_load.Controls.Add(this.comboBox_mapLoad_game);
            this.tabPage_load.Controls.Add(this.button_mapLoad_load);
            this.tabPage_load.Controls.Add(this.groupBox_mapLoad_status);
            this.tabPage_load.Controls.Add(this.label_mapLoad_addr);
            this.tabPage_load.Controls.Add(this.label_mapLoad_game);
            this.tabPage_load.Controls.Add(this.textBox_mapLoad_addr);
            this.tabPage_load.Controls.Add(this.label_mapLoad_map);
            this.tabPage_load.Controls.Add(this.label_mapLoad_gamemode);
            this.tabPage_load.Controls.Add(this.comboBox_mapLoad_map);
            this.tabPage_load.Controls.Add(this.comboBox_mapLoad_gamemode);
            this.tabPage_load.Location = new System.Drawing.Point(4, 22);
            this.tabPage_load.Name = "tabPage_load";
            this.tabPage_load.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_load.Size = new System.Drawing.Size(554, 472);
            this.tabPage_load.TabIndex = 0;
            this.tabPage_load.Text = "Forceload";
            this.tabPage_load.UseVisualStyleBackColor = true;
            // 
            // button_global_findXenia
            // 
            this.button_global_findXenia.Location = new System.Drawing.Point(3, 141);
            this.button_global_findXenia.Name = "button_global_findXenia";
            this.button_global_findXenia.Size = new System.Drawing.Size(98, 23);
            this.button_global_findXenia.TabIndex = 10;
            this.button_global_findXenia.Text = "Attach to process";
            this.button_global_findXenia.UseVisualStyleBackColor = true;
            this.button_global_findXenia.Click += new System.EventHandler(this.button_global_findXenia_Click);
            // 
            // comboBox_mapLoad_game
            // 
            this.comboBox_mapLoad_game.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_mapLoad_game.FormattingEnabled = true;
            this.comboBox_mapLoad_game.Location = new System.Drawing.Point(3, 6);
            this.comboBox_mapLoad_game.MaxDropDownItems = 2;
            this.comboBox_mapLoad_game.Name = "comboBox_mapLoad_game";
            this.comboBox_mapLoad_game.Size = new System.Drawing.Size(303, 21);
            this.comboBox_mapLoad_game.TabIndex = 1;
            this.comboBox_mapLoad_game.SelectedValueChanged += new System.EventHandler(this.comboBox_mapLoad_game_SelectedValueChanged);
            // 
            // button_mapLoad_load
            // 
            this.button_mapLoad_load.Location = new System.Drawing.Point(3, 112);
            this.button_mapLoad_load.Name = "button_mapLoad_load";
            this.button_mapLoad_load.Size = new System.Drawing.Size(75, 23);
            this.button_mapLoad_load.TabIndex = 9;
            this.button_mapLoad_load.Text = "Forceload";
            this.button_mapLoad_load.UseVisualStyleBackColor = true;
            this.button_mapLoad_load.Click += new System.EventHandler(this.button_mapLoad_load_Click);
            // 
            // groupBox_mapLoad_status
            // 
            this.groupBox_mapLoad_status.Controls.Add(this.label_mapLoad_status);
            this.groupBox_mapLoad_status.Location = new System.Drawing.Point(6, 431);
            this.groupBox_mapLoad_status.Name = "groupBox_mapLoad_status";
            this.groupBox_mapLoad_status.Size = new System.Drawing.Size(542, 35);
            this.groupBox_mapLoad_status.TabIndex = 0;
            this.groupBox_mapLoad_status.TabStop = false;
            this.groupBox_mapLoad_status.Text = "Status";
            // 
            // label_mapLoad_status
            // 
            this.label_mapLoad_status.AutoSize = true;
            this.label_mapLoad_status.Location = new System.Drawing.Point(6, 16);
            this.label_mapLoad_status.Name = "label_mapLoad_status";
            this.label_mapLoad_status.Size = new System.Drawing.Size(24, 13);
            this.label_mapLoad_status.TabIndex = 1;
            this.label_mapLoad_status.Text = "Idle";
            // 
            // label_mapLoad_addr
            // 
            this.label_mapLoad_addr.AutoSize = true;
            this.label_mapLoad_addr.Location = new System.Drawing.Point(312, 88);
            this.label_mapLoad_addr.Name = "label_mapLoad_addr";
            this.label_mapLoad_addr.Size = new System.Drawing.Size(82, 13);
            this.label_mapLoad_addr.TabIndex = 8;
            this.label_mapLoad_addr.Text = "Current Address";
            // 
            // label_mapLoad_game
            // 
            this.label_mapLoad_game.AutoSize = true;
            this.label_mapLoad_game.Location = new System.Drawing.Point(312, 7);
            this.label_mapLoad_game.Name = "label_mapLoad_game";
            this.label_mapLoad_game.Size = new System.Drawing.Size(35, 13);
            this.label_mapLoad_game.TabIndex = 2;
            this.label_mapLoad_game.Text = "Game";
            // 
            // textBox_mapLoad_addr
            // 
            this.textBox_mapLoad_addr.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_mapLoad_addr.Location = new System.Drawing.Point(3, 86);
            this.textBox_mapLoad_addr.Name = "textBox_mapLoad_addr";
            this.textBox_mapLoad_addr.Size = new System.Drawing.Size(303, 20);
            this.textBox_mapLoad_addr.TabIndex = 7;
            // 
            // label_mapLoad_map
            // 
            this.label_mapLoad_map.AutoSize = true;
            this.label_mapLoad_map.Location = new System.Drawing.Point(312, 34);
            this.label_mapLoad_map.Name = "label_mapLoad_map";
            this.label_mapLoad_map.Size = new System.Drawing.Size(28, 13);
            this.label_mapLoad_map.TabIndex = 3;
            this.label_mapLoad_map.Text = "Map";
            // 
            // label_mapLoad_gamemode
            // 
            this.label_mapLoad_gamemode.AutoSize = true;
            this.label_mapLoad_gamemode.Location = new System.Drawing.Point(312, 61);
            this.label_mapLoad_gamemode.Name = "label_mapLoad_gamemode";
            this.label_mapLoad_gamemode.Size = new System.Drawing.Size(61, 13);
            this.label_mapLoad_gamemode.TabIndex = 6;
            this.label_mapLoad_gamemode.Text = "Gamemode";
            // 
            // comboBox_mapLoad_map
            // 
            this.comboBox_mapLoad_map.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_mapLoad_map.FormattingEnabled = true;
            this.comboBox_mapLoad_map.ItemHeight = 13;
            this.comboBox_mapLoad_map.Location = new System.Drawing.Point(3, 32);
            this.comboBox_mapLoad_map.MaxDropDownItems = 50;
            this.comboBox_mapLoad_map.Name = "comboBox_mapLoad_map";
            this.comboBox_mapLoad_map.Size = new System.Drawing.Size(303, 21);
            this.comboBox_mapLoad_map.TabIndex = 4;
            // 
            // comboBox_mapLoad_gamemode
            // 
            this.comboBox_mapLoad_gamemode.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_mapLoad_gamemode.FormattingEnabled = true;
            this.comboBox_mapLoad_gamemode.Location = new System.Drawing.Point(3, 59);
            this.comboBox_mapLoad_gamemode.MaxDropDownItems = 5;
            this.comboBox_mapLoad_gamemode.Name = "comboBox_mapLoad_gamemode";
            this.comboBox_mapLoad_gamemode.Size = new System.Drawing.Size(303, 21);
            this.comboBox_mapLoad_gamemode.TabIndex = 5;
            // 
            // tabPage_memScan
            // 
            this.tabPage_memScan.Controls.Add(this.button3);
            this.tabPage_memScan.Controls.Add(this.groupBox_memSearch_status);
            this.tabPage_memScan.Controls.Add(button_memScan_findAll);
            this.tabPage_memScan.Controls.Add(this.textBox_memSearch_pattern);
            this.tabPage_memScan.Controls.Add(this.label_memScan_initAddr);
            this.tabPage_memScan.Controls.Add(button_memScan_findFirst);
            this.tabPage_memScan.Controls.Add(this.textBox_memScan_initAddr);
            this.tabPage_memScan.Controls.Add(this.button_memScan_stop);
            this.tabPage_memScan.Controls.Add(this.groupBox_memScan_results);
            this.tabPage_memScan.Location = new System.Drawing.Point(4, 22);
            this.tabPage_memScan.Name = "tabPage_memScan";
            this.tabPage_memScan.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_memScan.Size = new System.Drawing.Size(554, 472);
            this.tabPage_memScan.TabIndex = 1;
            this.tabPage_memScan.Text = "Scan";
            this.tabPage_memScan.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(154, 87);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Null All";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_scan_nullAllShaders);
            // 
            // tabPage_cache
            // 
            this.tabPage_cache.Controls.Add(this.button_cache_clearCheckedListBox);
            this.tabPage_cache.Controls.Add(this.button_cache_selectShaders);
            this.tabPage_cache.Controls.Add(this.button_cache_restore);
            this.tabPage_cache.Controls.Add(this.button_cache_backup);
            this.tabPage_cache.Controls.Add(this.richTextBox_cache_info);
            this.tabPage_cache.Controls.Add(this.button_cache_apply);
            this.tabPage_cache.Controls.Add(this.comboBox_cache_type);
            this.tabPage_cache.Controls.Add(this.checkedListBox_cache_tagClasses);
            this.tabPage_cache.Controls.Add(this.groupBox1);
            this.tabPage_cache.Controls.Add(this.button_cache_load);
            this.tabPage_cache.Location = new System.Drawing.Point(4, 22);
            this.tabPage_cache.Name = "tabPage_cache";
            this.tabPage_cache.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_cache.Size = new System.Drawing.Size(554, 472);
            this.tabPage_cache.TabIndex = 2;
            this.tabPage_cache.Text = "Cache file";
            this.tabPage_cache.UseVisualStyleBackColor = true;
            // 
            // button_cache_clearCheckedListBox
            // 
            this.button_cache_clearCheckedListBox.Location = new System.Drawing.Point(6, 152);
            this.button_cache_clearCheckedListBox.Name = "button_cache_clearCheckedListBox";
            this.button_cache_clearCheckedListBox.Size = new System.Drawing.Size(90, 23);
            this.button_cache_clearCheckedListBox.TabIndex = 11;
            this.button_cache_clearCheckedListBox.Text = "Clear";
            this.button_cache_clearCheckedListBox.UseVisualStyleBackColor = true;
            this.button_cache_clearCheckedListBox.Click += new System.EventHandler(this.button_cache_clearCheckedListBox_Click);
            // 
            // button_cache_selectShaders
            // 
            this.button_cache_selectShaders.Location = new System.Drawing.Point(6, 123);
            this.button_cache_selectShaders.Name = "button_cache_selectShaders";
            this.button_cache_selectShaders.Size = new System.Drawing.Size(90, 23);
            this.button_cache_selectShaders.TabIndex = 10;
            this.button_cache_selectShaders.Text = "Select Shaders";
            this.button_cache_selectShaders.UseVisualStyleBackColor = true;
            this.button_cache_selectShaders.Click += new System.EventHandler(this.button_cache_selectTagClasses);
            // 
            // button_cache_restore
            // 
            this.button_cache_restore.Location = new System.Drawing.Point(6, 94);
            this.button_cache_restore.Name = "button_cache_restore";
            this.button_cache_restore.Size = new System.Drawing.Size(90, 23);
            this.button_cache_restore.TabIndex = 9;
            this.button_cache_restore.Text = "Restore bkp";
            this.button_cache_restore.UseVisualStyleBackColor = true;
            this.button_cache_restore.Click += new System.EventHandler(this.button_cache_restore_Click);
            // 
            // button_cache_backup
            // 
            this.button_cache_backup.Location = new System.Drawing.Point(6, 65);
            this.button_cache_backup.Name = "button_cache_backup";
            this.button_cache_backup.Size = new System.Drawing.Size(90, 23);
            this.button_cache_backup.TabIndex = 8;
            this.button_cache_backup.Text = "Backup";
            this.button_cache_backup.UseVisualStyleBackColor = true;
            this.button_cache_backup.Click += new System.EventHandler(this.button_cache_backup_Click);
            // 
            // richTextBox_cache_info
            // 
            this.richTextBox_cache_info.Location = new System.Drawing.Point(6, 346);
            this.richTextBox_cache_info.Name = "richTextBox_cache_info";
            this.richTextBox_cache_info.Size = new System.Drawing.Size(542, 79);
            this.richTextBox_cache_info.TabIndex = 7;
            this.richTextBox_cache_info.Text = "";
            // 
            // button_cache_apply
            // 
            this.button_cache_apply.Location = new System.Drawing.Point(6, 181);
            this.button_cache_apply.Name = "button_cache_apply";
            this.button_cache_apply.Size = new System.Drawing.Size(90, 23);
            this.button_cache_apply.TabIndex = 6;
            this.button_cache_apply.Text = "Apply";
            this.button_cache_apply.UseVisualStyleBackColor = true;
            this.button_cache_apply.Click += new System.EventHandler(this.button_cache_apply_Click);
            // 
            // comboBox_cache_type
            // 
            this.comboBox_cache_type.FormattingEnabled = true;
            this.comboBox_cache_type.Location = new System.Drawing.Point(6, 35);
            this.comboBox_cache_type.Name = "comboBox_cache_type";
            this.comboBox_cache_type.Size = new System.Drawing.Size(90, 21);
            this.comboBox_cache_type.TabIndex = 5;
            // 
            // checkedListBox_cache_tagClasses
            // 
            this.checkedListBox_cache_tagClasses.CheckOnClick = true;
            this.checkedListBox_cache_tagClasses.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBox_cache_tagClasses.FormattingEnabled = true;
            this.checkedListBox_cache_tagClasses.Location = new System.Drawing.Point(102, 6);
            this.checkedListBox_cache_tagClasses.Name = "checkedListBox_cache_tagClasses";
            this.checkedListBox_cache_tagClasses.Size = new System.Drawing.Size(446, 334);
            this.checkedListBox_cache_tagClasses.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_cache_status);
            this.groupBox1.Location = new System.Drawing.Point(6, 431);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(542, 35);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status";
            // 
            // label_cache_status
            // 
            this.label_cache_status.AutoSize = true;
            this.label_cache_status.Location = new System.Drawing.Point(6, 16);
            this.label_cache_status.Name = "label_cache_status";
            this.label_cache_status.Size = new System.Drawing.Size(24, 13);
            this.label_cache_status.TabIndex = 1;
            this.label_cache_status.Text = "Idle";
            // 
            // button_cache_load
            // 
            this.button_cache_load.Location = new System.Drawing.Point(6, 6);
            this.button_cache_load.Name = "button_cache_load";
            this.button_cache_load.Size = new System.Drawing.Size(90, 23);
            this.button_cache_load.TabIndex = 0;
            this.button_cache_load.Text = "Select map file";
            this.button_cache_load.UseVisualStyleBackColor = true;
            this.button_cache_load.Click += new System.EventHandler(this.button_cache_load_Click);
            // 
            // tabPage_camera
            // 
            this.tabPage_camera.Controls.Add(this.button2);
            this.tabPage_camera.Controls.Add(this.button1);
            this.tabPage_camera.Location = new System.Drawing.Point(4, 22);
            this.tabPage_camera.Name = "tabPage_camera";
            this.tabPage_camera.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_camera.Size = new System.Drawing.Size(554, 472);
            this.tabPage_camera.TabIndex = 3;
            this.tabPage_camera.Text = "Camera";
            this.tabPage_camera.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "test";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_camera_2);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "camera test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_camera_1);
            // 
            // tabPage_rasg
            // 
            this.tabPage_rasg.Controls.Add(this.button5);
            this.tabPage_rasg.Controls.Add(this.checkedListBox_rasg_shaderReplacement);
            this.tabPage_rasg.Controls.Add(this.checkedListBox_rasg_shaders_selection);
            this.tabPage_rasg.Controls.Add(this.button_rmt2_reloadMap);
            this.tabPage_rasg.Controls.Add(this.checkBox_tag_autoPoke);
            this.tabPage_rasg.Controls.Add(this.button4);
            this.tabPage_rasg.Controls.Add(this.numericUpDown2);
            this.tabPage_rasg.Controls.Add(this.numericUpDown1);
            this.tabPage_rasg.Controls.Add(this.label2);
            this.tabPage_rasg.Controls.Add(this.label1);
            this.tabPage_rasg.Controls.Add(this.groupBox_tag_status);
            this.tabPage_rasg.Controls.Add(this.button_tag_replace);
            this.tabPage_rasg.Controls.Add(this.comboBox_tag_replacement);
            this.tabPage_rasg.Controls.Add(this.comboBox_tag_selection);
            this.tabPage_rasg.Controls.Add(this.textBox_tag_search);
            this.tabPage_rasg.Controls.Add(this.button_tag_search);
            this.tabPage_rasg.Location = new System.Drawing.Point(4, 22);
            this.tabPage_rasg.Name = "tabPage_rasg";
            this.tabPage_rasg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_rasg.Size = new System.Drawing.Size(554, 472);
            this.tabPage_rasg.TabIndex = 4;
            this.tabPage_rasg.Text = "rasg";
            this.tabPage_rasg.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(168, 400);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "Clear";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // checkedListBox_rasg_shaderReplacement
            // 
            this.checkedListBox_rasg_shaderReplacement.CheckOnClick = true;
            this.checkedListBox_rasg_shaderReplacement.FormattingEnabled = true;
            this.checkedListBox_rasg_shaderReplacement.Location = new System.Drawing.Point(280, 85);
            this.checkedListBox_rasg_shaderReplacement.Name = "checkedListBox_rasg_shaderReplacement";
            this.checkedListBox_rasg_shaderReplacement.Size = new System.Drawing.Size(268, 304);
            this.checkedListBox_rasg_shaderReplacement.TabIndex = 13;
            // 
            // checkedListBox_rasg_shaders_selection
            // 
            this.checkedListBox_rasg_shaders_selection.CheckOnClick = true;
            this.checkedListBox_rasg_shaders_selection.FormattingEnabled = true;
            this.checkedListBox_rasg_shaders_selection.Location = new System.Drawing.Point(6, 85);
            this.checkedListBox_rasg_shaders_selection.Name = "checkedListBox_rasg_shaders_selection";
            this.checkedListBox_rasg_shaders_selection.Size = new System.Drawing.Size(268, 304);
            this.checkedListBox_rasg_shaders_selection.TabIndex = 12;
            this.checkedListBox_rasg_shaders_selection.SelectedIndexChanged += new System.EventHandler(this.checkedListBox_rasg_shaders_selection_SelectedIndexChanged);
            // 
            // button_rmt2_reloadMap
            // 
            this.button_rmt2_reloadMap.Location = new System.Drawing.Point(87, 400);
            this.button_rmt2_reloadMap.Name = "button_rmt2_reloadMap";
            this.button_rmt2_reloadMap.Size = new System.Drawing.Size(75, 23);
            this.button_rmt2_reloadMap.TabIndex = 11;
            this.button_rmt2_reloadMap.Text = "Reload map";
            this.button_rmt2_reloadMap.UseVisualStyleBackColor = true;
            this.button_rmt2_reloadMap.Click += new System.EventHandler(this.button_rasg_reloadMap_Click);
            // 
            // checkBox_tag_autoPoke
            // 
            this.checkBox_tag_autoPoke.AutoSize = true;
            this.checkBox_tag_autoPoke.Location = new System.Drawing.Point(473, 8);
            this.checkBox_tag_autoPoke.Name = "checkBox_tag_autoPoke";
            this.checkBox_tag_autoPoke.Size = new System.Drawing.Size(75, 17);
            this.checkBox_tag_autoPoke.TabIndex = 10;
            this.checkBox_tag_autoPoke.Text = "Auto poke";
            this.checkBox_tag_autoPoke.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(240, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(66, 23);
            this.button4.TabIndex = 9;
            this.button4.Text = "Reset";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_rasg_reset);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(435, 61);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            164,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(37, 20);
            this.numericUpDown2.TabIndex = 8;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(435, 33);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            164,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(37, 20);
            this.numericUpDown1.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(478, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Replacement";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(478, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Selected";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox_tag_status
            // 
            this.groupBox_tag_status.Controls.Add(this.label_tag_status);
            this.groupBox_tag_status.Location = new System.Drawing.Point(6, 429);
            this.groupBox_tag_status.Name = "groupBox_tag_status";
            this.groupBox_tag_status.Size = new System.Drawing.Size(542, 37);
            this.groupBox_tag_status.TabIndex = 5;
            this.groupBox_tag_status.TabStop = false;
            this.groupBox_tag_status.Text = "Status";
            // 
            // label_tag_status
            // 
            this.label_tag_status.AutoSize = true;
            this.label_tag_status.Location = new System.Drawing.Point(6, 16);
            this.label_tag_status.Name = "label_tag_status";
            this.label_tag_status.Size = new System.Drawing.Size(24, 13);
            this.label_tag_status.TabIndex = 0;
            this.label_tag_status.Text = "Idle";
            // 
            // button_tag_replace
            // 
            this.button_tag_replace.Location = new System.Drawing.Point(6, 400);
            this.button_tag_replace.Name = "button_tag_replace";
            this.button_tag_replace.Size = new System.Drawing.Size(75, 23);
            this.button_tag_replace.TabIndex = 4;
            this.button_tag_replace.Text = "Apply";
            this.button_tag_replace.UseVisualStyleBackColor = true;
            this.button_tag_replace.Click += new System.EventHandler(this.button_rasg_apply);
            // 
            // comboBox_tag_replacement
            // 
            this.comboBox_tag_replacement.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_tag_replacement.FormattingEnabled = true;
            this.comboBox_tag_replacement.Location = new System.Drawing.Point(6, 59);
            this.comboBox_tag_replacement.Name = "comboBox_tag_replacement";
            this.comboBox_tag_replacement.Size = new System.Drawing.Size(423, 21);
            this.comboBox_tag_replacement.TabIndex = 3;
            this.comboBox_tag_replacement.SelectedIndexChanged += new System.EventHandler(this.comboBox_rasg_replacement_SelectedIndexChanged);
            // 
            // comboBox_tag_selection
            // 
            this.comboBox_tag_selection.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_tag_selection.FormattingEnabled = true;
            this.comboBox_tag_selection.Location = new System.Drawing.Point(6, 32);
            this.comboBox_tag_selection.Name = "comboBox_tag_selection";
            this.comboBox_tag_selection.Size = new System.Drawing.Size(423, 21);
            this.comboBox_tag_selection.TabIndex = 2;
            this.comboBox_tag_selection.SelectedIndexChanged += new System.EventHandler(this.comboBox_rasg_selection_SelectedIndexChanged);
            // 
            // textBox_tag_search
            // 
            this.textBox_tag_search.Location = new System.Drawing.Point(6, 6);
            this.textBox_tag_search.Name = "textBox_tag_search";
            this.textBox_tag_search.Size = new System.Drawing.Size(147, 20);
            this.textBox_tag_search.TabIndex = 1;
            // 
            // button_tag_search
            // 
            this.button_tag_search.Location = new System.Drawing.Point(159, 4);
            this.button_tag_search.Name = "button_tag_search";
            this.button_tag_search.Size = new System.Drawing.Size(75, 23);
            this.button_tag_search.TabIndex = 0;
            this.button_tag_search.Text = "Initialize";
            this.button_tag_search.UseVisualStyleBackColor = true;
            this.button_tag_search.Click += new System.EventHandler(this.button_rasg_search_Click);
            // 
            // tabPage_rmt2
            // 
            this.tabPage_rmt2.Controls.Add(this.button_tabRmt2_clearFlags);
            this.tabPage_rmt2.Controls.Add(this.button_tabRmt2_reloadMap);
            this.tabPage_rmt2.Controls.Add(this.button_tabRmt2_setOne);
            this.tabPage_rmt2.Controls.Add(this.checkedListBox_tabRmt2_flags);
            this.tabPage_rmt2.Controls.Add(this.textBox_tabRmt2_address);
            this.tabPage_rmt2.Location = new System.Drawing.Point(4, 22);
            this.tabPage_rmt2.Name = "tabPage_rmt2";
            this.tabPage_rmt2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_rmt2.Size = new System.Drawing.Size(554, 472);
            this.tabPage_rmt2.TabIndex = 5;
            this.tabPage_rmt2.Text = "rmt2";
            this.tabPage_rmt2.UseVisualStyleBackColor = true;
            // 
            // button_tabRmt2_reloadMap
            // 
            this.button_tabRmt2_reloadMap.Location = new System.Drawing.Point(228, 4);
            this.button_tabRmt2_reloadMap.Name = "button_tabRmt2_reloadMap";
            this.button_tabRmt2_reloadMap.Size = new System.Drawing.Size(75, 23);
            this.button_tabRmt2_reloadMap.TabIndex = 6;
            this.button_tabRmt2_reloadMap.Text = "Reload map";
            this.button_tabRmt2_reloadMap.UseVisualStyleBackColor = true;
            this.button_tabRmt2_reloadMap.Click += new System.EventHandler(this.button_tabRmt2_reloadMap_Click);
            // 
            // button_tabRmt2_setOne
            // 
            this.button_tabRmt2_setOne.Location = new System.Drawing.Point(147, 4);
            this.button_tabRmt2_setOne.Name = "button_tabRmt2_setOne";
            this.button_tabRmt2_setOne.Size = new System.Drawing.Size(75, 23);
            this.button_tabRmt2_setOne.TabIndex = 3;
            this.button_tabRmt2_setOne.Text = "Set All";
            this.button_tabRmt2_setOne.UseVisualStyleBackColor = true;
            this.button_tabRmt2_setOne.Click += new System.EventHandler(this.button_rmt2_setOne);
            // 
            // checkedListBox_tabRmt2_flags
            // 
            this.checkedListBox_tabRmt2_flags.CheckOnClick = true;
            this.checkedListBox_tabRmt2_flags.FormattingEnabled = true;
            this.checkedListBox_tabRmt2_flags.Location = new System.Drawing.Point(6, 32);
            this.checkedListBox_tabRmt2_flags.Name = "checkedListBox_tabRmt2_flags";
            this.checkedListBox_tabRmt2_flags.Size = new System.Drawing.Size(135, 364);
            this.checkedListBox_tabRmt2_flags.TabIndex = 2;
            this.checkedListBox_tabRmt2_flags.SelectedIndexChanged += new System.EventHandler(this.checkedListBox_tab3_flags_SelectedIndexChanged);
            // 
            // textBox_tabRmt2_address
            // 
            this.textBox_tabRmt2_address.Location = new System.Drawing.Point(6, 6);
            this.textBox_tabRmt2_address.Name = "textBox_tabRmt2_address";
            this.textBox_tabRmt2_address.Size = new System.Drawing.Size(135, 20);
            this.textBox_tabRmt2_address.TabIndex = 0;
            this.textBox_tabRmt2_address.TextChanged += new System.EventHandler(this.textBox_rasg_address_TextChanged);
            // 
            // tabPage_shaders
            // 
            this.tabPage_shaders.Controls.Add(this.checkBox_shaders_autopoke);
            this.tabPage_shaders.Controls.Add(this.comboBox_shaders_replacement);
            this.tabPage_shaders.Controls.Add(this.label12);
            this.tabPage_shaders.Controls.Add(this.button22);
            this.tabPage_shaders.Controls.Add(this.button21);
            this.tabPage_shaders.Controls.Add(this.button20);
            this.tabPage_shaders.Controls.Add(this.label11);
            this.tabPage_shaders.Controls.Add(this.label10);
            this.tabPage_shaders.Controls.Add(this.label9);
            this.tabPage_shaders.Controls.Add(this.label8);
            this.tabPage_shaders.Controls.Add(this.label7);
            this.tabPage_shaders.Controls.Add(this.label6);
            this.tabPage_shaders.Controls.Add(this.label5);
            this.tabPage_shaders.Controls.Add(this.label4);
            this.tabPage_shaders.Controls.Add(this.label3);
            this.tabPage_shaders.Controls.Add(this.button13);
            this.tabPage_shaders.Controls.Add(this.button14);
            this.tabPage_shaders.Controls.Add(this.button15);
            this.tabPage_shaders.Controls.Add(this.button16);
            this.tabPage_shaders.Controls.Add(this.button17);
            this.tabPage_shaders.Controls.Add(this.button18);
            this.tabPage_shaders.Controls.Add(this.button19);
            this.tabPage_shaders.Controls.Add(this.button12);
            this.tabPage_shaders.Controls.Add(this.button11);
            this.tabPage_shaders.Controls.Add(this.button10);
            this.tabPage_shaders.Controls.Add(this.button9);
            this.tabPage_shaders.Controls.Add(this.button8);
            this.tabPage_shaders.Controls.Add(this.button7);
            this.tabPage_shaders.Controls.Add(this.button6);
            this.tabPage_shaders.Location = new System.Drawing.Point(4, 22);
            this.tabPage_shaders.Name = "tabPage_shaders";
            this.tabPage_shaders.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_shaders.Size = new System.Drawing.Size(554, 472);
            this.tabPage_shaders.TabIndex = 6;
            this.tabPage_shaders.Text = "Shaders";
            this.tabPage_shaders.UseVisualStyleBackColor = true;
            // 
            // checkBox_shaders_autopoke
            // 
            this.checkBox_shaders_autopoke.AutoSize = true;
            this.checkBox_shaders_autopoke.Location = new System.Drawing.Point(87, 280);
            this.checkBox_shaders_autopoke.Name = "checkBox_shaders_autopoke";
            this.checkBox_shaders_autopoke.Size = new System.Drawing.Size(72, 17);
            this.checkBox_shaders_autopoke.TabIndex = 36;
            this.checkBox_shaders_autopoke.Text = "Autopoke";
            this.checkBox_shaders_autopoke.UseVisualStyleBackColor = true;
            this.checkBox_shaders_autopoke.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // comboBox_shaders_replacement
            // 
            this.comboBox_shaders_replacement.FormattingEnabled = true;
            this.comboBox_shaders_replacement.Location = new System.Drawing.Point(6, 251);
            this.comboBox_shaders_replacement.Name = "comboBox_shaders_replacement";
            this.comboBox_shaders_replacement.Size = new System.Drawing.Size(542, 21);
            this.comboBox_shaders_replacement.TabIndex = 35;
            this.comboBox_shaders_replacement.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(167, 227);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "all";
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(87, 222);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 32;
            this.button22.Text = "button22";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(6, 222);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 31;
            this.button21.Text = "button21";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(6, 276);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 30;
            this.button20.Text = "Reload map";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button_tabShaders_reloadMap_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(104, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "replace";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "null";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(168, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "rmd";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(168, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "rmw";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(168, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "rmhg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(168, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "rmtr";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(168, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "rmfl";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(168, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "rmgl";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(168, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "rmsh";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(87, 193);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 13;
            this.button13.Text = "button13";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(87, 164);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 12;
            this.button14.Text = "button14";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(87, 135);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 11;
            this.button15.Text = "button15";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(87, 106);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 10;
            this.button16.Text = "button16";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(87, 77);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 9;
            this.button17.Text = "button17";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(87, 48);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 8;
            this.button18.Text = "button18";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(87, 19);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 7;
            this.button19.Text = "button19";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(6, 193);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 6;
            this.button12.Text = "button12";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(6, 164);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 5;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(6, 135);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 4;
            this.button10.Text = "button10";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(6, 106);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 3;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(6, 77);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 2;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(6, 48);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 1;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(6, 19);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 0;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button_tabRmt2_clearFlags
            // 
            this.button_tabRmt2_clearFlags.Location = new System.Drawing.Point(309, 4);
            this.button_tabRmt2_clearFlags.Name = "button_tabRmt2_clearFlags";
            this.button_tabRmt2_clearFlags.Size = new System.Drawing.Size(75, 23);
            this.button_tabRmt2_clearFlags.TabIndex = 7;
            this.button_tabRmt2_clearFlags.Text = "Clear flags";
            this.button_tabRmt2_clearFlags.UseVisualStyleBackColor = true;
            this.button_tabRmt2_clearFlags.Click += new System.EventHandler(this.button_tabRmt2_clearFlags_Click);
            this.button_tabRmt2_clearFlags.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button_tabRmt2_clearFlags_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 540);
            this.Controls.Add(this.tabControl);
            this.Name = "Form1";
            this.Text = "Xenia Tool: Halo map forceloader";
            this.groupBox_memScan_results.ResumeLayout(false);
            this.groupBox_memSearch_status.ResumeLayout(false);
            this.groupBox_memSearch_status.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage_load.ResumeLayout(false);
            this.tabPage_load.PerformLayout();
            this.groupBox_mapLoad_status.ResumeLayout(false);
            this.groupBox_mapLoad_status.PerformLayout();
            this.tabPage_memScan.ResumeLayout(false);
            this.tabPage_memScan.PerformLayout();
            this.tabPage_cache.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage_camera.ResumeLayout(false);
            this.tabPage_rasg.ResumeLayout(false);
            this.tabPage_rasg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox_tag_status.ResumeLayout(false);
            this.groupBox_tag_status.PerformLayout();
            this.tabPage_rmt2.ResumeLayout(false);
            this.tabPage_rmt2.PerformLayout();
            this.tabPage_shaders.ResumeLayout(false);
            this.tabPage_shaders.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_memSearch_status;
        private System.Windows.Forms.Label label_memSearch_status;
        private System.Windows.Forms.GroupBox groupBox_memScan_results;
        private System.Windows.Forms.RichTextBox richTextBox_memScan_results;
        private System.Windows.Forms.Button button_memScan_stop;
        private System.Windows.Forms.TextBox textBox_memSearch_pattern;
        private System.Windows.Forms.TextBox textBox_memScan_initAddr;
        private System.Windows.Forms.Label label_memScan_initAddr;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage_load;
        private System.Windows.Forms.TabPage tabPage_memScan;
        private System.Windows.Forms.GroupBox groupBox_mapLoad_status;
        private System.Windows.Forms.Label label_mapLoad_status;
        private System.Windows.Forms.ComboBox comboBox_mapLoad_game;
        private System.Windows.Forms.Label label_mapLoad_game;
        private System.Windows.Forms.Label label_mapLoad_map;
        private System.Windows.Forms.ComboBox comboBox_mapLoad_map;
        private System.Windows.Forms.ComboBox comboBox_mapLoad_gamemode;
        private System.Windows.Forms.Label label_mapLoad_gamemode;
        private System.Windows.Forms.TextBox textBox_mapLoad_addr;
        private System.Windows.Forms.Label label_mapLoad_addr;
        private System.Windows.Forms.Button button_mapLoad_load;
        private System.Windows.Forms.TabPage tabPage_cache;
        private System.Windows.Forms.Button button_cache_load;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_cache_status;
        private System.Windows.Forms.CheckedListBox checkedListBox_cache_tagClasses;
        private System.Windows.Forms.ComboBox comboBox_cache_type;
        private System.Windows.Forms.Button button_cache_apply;
        private System.Windows.Forms.RichTextBox richTextBox_cache_info;
        private System.Windows.Forms.Button button_cache_restore;
        private System.Windows.Forms.Button button_cache_backup;
        private System.Windows.Forms.Button button_cache_selectShaders;
        private System.Windows.Forms.Button button_cache_clearCheckedListBox;
        private System.Windows.Forms.TabPage tabPage_camera;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage_rasg;
        private System.Windows.Forms.Button button_tag_replace;
        private System.Windows.Forms.ComboBox comboBox_tag_replacement;
        private System.Windows.Forms.ComboBox comboBox_tag_selection;
        private System.Windows.Forms.TextBox textBox_tag_search;
        private System.Windows.Forms.Button button_tag_search;
        private System.Windows.Forms.GroupBox groupBox_tag_status;
        private System.Windows.Forms.Label label_tag_status;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox checkBox_tag_autoPoke;
        private System.Windows.Forms.TabPage tabPage_rmt2;
        private System.Windows.Forms.TextBox textBox_tabRmt2_address;
        private System.Windows.Forms.CheckedListBox checkedListBox_tabRmt2_flags;
        private System.Windows.Forms.Button button_global_findXenia;
        private System.Windows.Forms.Button button_tabRmt2_setOne;
        private System.Windows.Forms.CheckedListBox checkedListBox_rasg_shaders_selection;
        private System.Windows.Forms.Button button_rmt2_reloadMap;
        private System.Windows.Forms.CheckedListBox checkedListBox_rasg_shaderReplacement;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TabPage tabPage_shaders;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.ComboBox comboBox_shaders_replacement;
        private System.Windows.Forms.CheckBox checkBox_shaders_autopoke;
        private System.Windows.Forms.Button button_tabRmt2_reloadMap;
        private System.Windows.Forms.Button button_tabRmt2_clearFlags;
    }
}

