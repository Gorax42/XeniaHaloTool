Features:
- Force load Halo 3 or Halo 3 ODST maps.
- Basic memory scan for a byte array.

Wishlist:
- Memory viewer and editor.
- Sliders.
- Tools for Halo 3 and Halo 3 ODST cache files to remove shaders, textures, other tags.
- Add support for Halo Reach.